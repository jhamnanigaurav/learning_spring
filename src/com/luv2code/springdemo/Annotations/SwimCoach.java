package com.luv2code.springdemo.Annotations;

import org.springframework.beans.factory.annotation.Value;

public class SwimCoach implements Coach {

	@Value("${name}")
	private String name;
	private FortuneService fortuneService;
	
	public SwimCoach(FortuneService fortuneService) {
		
		this.fortuneService = fortuneService;
	}

	@Override
	public String getDailyWorkout() {
		
		return name + " Do 10 laps of pool\n" +
				fortuneService.getFortune();
	}

}
