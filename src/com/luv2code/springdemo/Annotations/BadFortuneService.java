package com.luv2code.springdemo.Annotations;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

public class BadFortuneService implements FortuneService {
	
	@Value("${fortune3}")
	private String day;

	
	public String getFortune() {
		
		return "Alas! You might have a " + day
				+ " day.";
	}

}
