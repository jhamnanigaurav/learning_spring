package com.luv2code.springdemo.Annotations;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class SpringConfigJavaCodeApp {

	public static void main(String[] args) {
		

		AnnotationConfigApplicationContext context =
				new AnnotationConfigApplicationContext(
				com.luv2code.springdemo.Annotations.SpringConfigJavaCode.class);
		
		Coach ob = context.getBean("swimCoach", Coach.class);
		System.out.println(ob.getDailyWorkout());
		context.close();
	}

}
