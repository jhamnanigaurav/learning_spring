package com.luv2code.springdemo.Annotations;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("com/luv2code/springdemo/Annotations/refactor.Properties")
public class SpringConfigJavaCode {
	
	@Bean
	public Coach swimCoach() {
		return new SwimCoach(bad());
	}
	
	@Bean
	public FortuneService bad() {
		return new BadFortuneService();
	}

	

}
