package com.luv2code.springdemo.Annotations;

public interface Coach {
	
	public String getDailyWorkout();

}
