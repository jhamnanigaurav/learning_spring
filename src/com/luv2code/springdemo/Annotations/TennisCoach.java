package com.luv2code.springdemo.Annotations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

@Component
@PropertySource("com/luv2code/springdemo/Annotations/refactor.Properties")
public class TennisCoach implements Coach {
	
	@Autowired
	@Qualifier("uncertainFortuneService")
	private FortuneService fortuneService;
	
	@Value("${name}")
	private String name;
	@Value("${email}")
	private String email;
	
	public TennisCoach() {
		System.out.println("Inside TennisCoach Constructor");
	}
	
	/*
	// Testing setter injection(can use any method name)
	@Autowired
	@Qualifier("badFortuneService")
	public void setFortuneService(FortuneService fortuneService) {
		this.fortuneService = fortuneService;
	}*/
	
	/*
	// Testing constructor injection
	@Autowired
	//@Qualifier("uncertainFortuneService") << WRONG SYNTAX
	public TennisCoach(@Qualifier("uncertainFortuneService")FortuneService fortuneService) {
		this.fortuneService = fortuneService;
	}*/
	
	
	
	
	// Defining redundant method using IoC (without DI)
	
	/*private void setUpFortuneService() {
		
		ClassPathXmlApplicationContext ob =
				new ClassPathXmlApplicationContext(
				"/com/luv2code/springdemo/Annotations/applicationContext.xml");
		fortuneService = ob.getBean(
				"happyFortuneService", FortuneService.class);
		
	}*/

	@Override
	public String getDailyWorkout() {
		
		//setUpFortuneService();
		return name + " " + email +  
				"Look up backhand technique\n" + 
				fortuneService.getFortune();
	}

}
