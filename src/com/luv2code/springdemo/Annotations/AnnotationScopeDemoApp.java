package com.luv2code.springdemo.Annotations;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AnnotationScopeDemoApp {

	public static void main(String[] args) {
		
		ClassPathXmlApplicationContext context = 
				new ClassPathXmlApplicationContext(
				"com/luv2code/springdemo/Annotations/applicationContext.xml");
		Coach ob1 = context.getBean("tennisCoach", Coach.class);
		Coach ob2 = context.getBean("tennisCoach", Coach.class);

		System.out.println("Are two objects the same instance: " + 
		(ob1 == ob2));
		
		System.out.println(ob1.getDailyWorkout());
		
		System.out.println(ob2.getDailyWorkout());
		
		context.close();
	}

}
