package com.luv2code.springdemo.Annotations;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ConfigDemoApp {

	public static void main(String[] args) {
		
		
		// read spring config file
		
		AnnotationConfigApplicationContext context = 
				new AnnotationConfigApplicationContext(
				com.luv2code.springdemo.Annotations.SpringConfig.class);
		
		//	get a bean from Spring container
		
		Coach ob = context.getBean("tennisCoach", Coach.class);
		
		
		//	call a method on the bean
		
		System.out.println(ob.getDailyWorkout());
		
		//System.out.println((new TennisCoach()).getDailyWorkout());
		//	close the container
		
		context.close();

	}

}
