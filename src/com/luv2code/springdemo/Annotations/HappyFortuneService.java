package com.luv2code.springdemo.Annotations;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class HappyFortuneService implements FortuneService {

	
	@Value("$(fortune1)")
	private String day;
	
	public HappyFortuneService() {
		System.out.println("Inside HappyFortuneService constructor");
	}
	@Override
	public String getFortune() {
		
		return "You will have an amazing " + day
				+ "day";
	}

}
