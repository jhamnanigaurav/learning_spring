package com.luv2code.springdemo.Annotations;

import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
public class UncertainFortuneService implements FortuneService {

	@Value("${fortune2}")
	private String day;
	
	public int callCount = 0; // For figuring out scope of the beans
	
	@Override
	public String getFortune() {
		
		return day;
				
	}
	
	@PreDestroy
	public void countPrint() {
		
		System.out.println("This is a predest method\nCall count = " + (++callCount));
		
	}

}
