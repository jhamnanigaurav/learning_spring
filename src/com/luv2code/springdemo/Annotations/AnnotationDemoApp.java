package com.luv2code.springdemo.Annotations;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AnnotationDemoApp {

	public static void main(String[] args) {
		
		
		// read spring config file
		
		ClassPathXmlApplicationContext context = 
				new ClassPathXmlApplicationContext(
				"com/luv2code/springdemo/Annotations/applicationContext.xml");
		
		//	get a bean from Spring container
		
		Coach ob = context.getBean("tennisCoach", Coach.class);
		
		
		//	call a method on the bean
		
		System.out.println(ob.getDailyWorkout());
		
		//System.out.println((new TennisCoach()).getDailyWorkout());
		//	close the container
		
		context.close();

	}

}
