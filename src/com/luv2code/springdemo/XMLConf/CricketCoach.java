package com.luv2code.springdemo.XMLConf;

public class CricketCoach implements Coach{

	private FortuneService fortuneService;
	
	public CricketCoach() {
		System.out.println("Inside Cricket Coach constructor");
	}
	
	@Override
	public String getDailyWorkout() {
		
		return "Do a warmup and get to the nets";
	}

	@Override
	public String getFortune() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setFortuneService(FortuneService fortuneService) {
		System.out.println("Inside setter method");
		this.fortuneService = fortuneService;
	}
	
	

	
	
	

}
