package com.luv2code.springdemo.XMLConf;

public interface Coach {

	public String getDailyWorkout();
	
	public String getFortune();
}
