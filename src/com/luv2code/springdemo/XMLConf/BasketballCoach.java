package com.luv2code.springdemo.XMLConf;

import org.springframework.beans.factory.DisposableBean;

public class BasketballCoach implements Coach, DisposableBean {

	private String name;
	private String email;
	private int count = 0;
	private FortuneService fortuneService;
	
	
	public void setName(String name) {
		this.name = name;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String getDailyWorkout() {
		// TODO Auto-generated method stub
		count++;
		System.out.println("Function called for "+count+" time");
		return "Do 10 consecutive baskets "+name+" "+email;
		
	}
	
	public void setFortuneService(FortuneService fortuneService) {
		this.fortuneService = fortuneService;
	}
	@Override
	public String getFortune() {
		// TODO Auto-generated method stub
		return fortuneService.getFortune();
	}
	
	public void startUpStuff() {
		
		System.out.println("Mem In");
		
	}
	
	@Override
	public void destroy() {
		
		System.out.println("Mem Over");
		
	}

}
