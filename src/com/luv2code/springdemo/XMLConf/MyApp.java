package com.luv2code.springdemo.XMLConf;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MyApp {

	public static void main(String[] args) {
		
		ClassPathXmlApplicationContext context = 
				new ClassPathXmlApplicationContext("applicationContext.xml");
		
		BasketballCoach coachObj = context.getBean("basky", BasketballCoach.class);
		
		System.out.println(coachObj.getDailyWorkout());
		
		context.close();

	}

}
