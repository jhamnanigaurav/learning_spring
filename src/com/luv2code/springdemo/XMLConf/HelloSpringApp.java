package com.luv2code.springdemo.XMLConf;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class HelloSpringApp {

	public static void main(String[] args) {
		// load configuration file
		ClassPathXmlApplicationContext context = 
				new ClassPathXmlApplicationContext("applicationContext.xml");
		
		// retrieve bean from spring container
		
		Coach theCoach = context.getBean("basky", Coach.class);
		
		BasketballCoach anCoach = context.getBean("basky", BasketballCoach.class);
		// call methods on the bean
		System.out.println(theCoach.getDailyWorkout());
		System.out.println(theCoach.getFortune());
		System.out.println();
		
		
		System.out.println(anCoach.getDailyWorkout());
		System.out.println(anCoach.getFortune());
		
		// close the context
		
		context.close();

	}

}
