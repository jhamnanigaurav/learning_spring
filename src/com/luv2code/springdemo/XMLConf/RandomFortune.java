package com.luv2code.springdemo.XMLConf;

import java.util.Random;

public class RandomFortune implements FortuneService {

	public int count=0;
	@Override
	public String getFortune() {
		String[] arr = {"Good", "Bad", "Amazing"};
		Random ranObject = new Random();
		int rn = ranObject.nextInt(3);
		return arr[(count++)%3];
	}

}
